'use strict';

/**
 * Manages Views and their data.
 */
angular.module ( 'WebSockets', [] )
	.controller ( 'WSCtrl', [ '$scope', '$log', function ( $scope, $log ) {

		/**
		 * Each component initializes itself with their name to lookup url[name]
		 * @param name
		 */
		$scope.initName = function ( name ) {
			$scope.name = name;
		}

		/**
		 *
		 */
		$scope.$on ( 'ws.connected', function ( ws, data ) {
			$scope.ws = new WebSocket ( data.webSockets[$scope.name] );
			$scope.ws.onmessage = function ( msg ) {
				if ( msg.data.indexOf ( "timestamp" ) == -1 ) {
					var data = angular.fromJson ( msg.data );
					$scope.data.push ( data );
					$scope.$apply ();
				}
			};
		} );

		/**
		 * Object-Array containing data
		 * @type {Array}
		 */
		$scope.data = [];

	}] );