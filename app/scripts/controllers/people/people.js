'use strict';

angular.module ( 'People', [ 'REST' ] )
	.controller ( 'PersonCtrl', [ '$scope', '$routeParams', 'PeopleResource', function ( $scope, $routeParams, PeopleResource ) {

		$scope.data = {};
		$scope.peopleId = $routeParams.peopleId;

		PeopleResource.getById ( $scope.peopleId ).then ( function ( data ) {
			$scope.data = data;
		} );

		PeopleResource.getPublications ( $scope.peopleId ).then ( function ( data ) {
			$scope.data.publications = data;
		} );

		PeopleResource.getConferences ( $scope.peopleId ).then ( function ( data ) {
			$scope.data.conferences = data;
		} );

	}] )
	.controller ( 'PeopleCtrl', [ '$scope', '$location', 'PeopleResource', function ( $scope, $location, PeopleResource ) {

		$scope.data = [];
		$scope.page = 0;
		$scope.search = $location.search ();

		$scope.changePage = function ( page ) {
			page = parseInt ( page );
			PeopleResource.getAll ( page ).then ( function ( data ) {
				$scope.data = data;
				$scope.page = page;
				$location.path ( $location.path () ).search ( {page: page} );
			} );
		}

		if ( !$scope.search.page ) {
			$scope.changePage ( $scope.page );
		} else {
			$scope.changePage ( $scope.search.page );
		}

	}] )
	.factory ( 'PeopleResource', [ '$http', 'resources', function ( $http, resources ) {
		var all = {};
		var ids = {};
		var publications = {};
		var conferences = {};
		var people = {
			getAll         : function ( page ) {
				if ( !page ) {
					page = 0;
				}
				if ( !all[page] ) {
					all[page] = $http.get ( resources.people + "?page=" + page ).then ( function ( response ) {
						return response.data;
					} );
				}
				return all[page];
			},
			getById        : function ( id ) {
				if ( id ) {
					if ( !ids[id] ) {
						ids[id] = $http.get ( resources.people + "/" + id ).then ( function ( response ) {
							return response.data;
						} );
					}
					return ids[id];
				}
			},
			getPublications: function ( id ) {
				if ( id ) {
					if ( !publications[id] ) {
						publications[id] = $http.get ( resources.people + "/" + id + "/publications" ).then ( function ( response ) {
							return response.data;
						} );
					}
					return publications[id];
				}
			},
			getConferences : function ( id ) {
				if ( id ) {
					if ( !conferences[id] ) {
						conferences[id] = $http.get ( resources.people + "/" + id + "/conferences" ).then ( function ( response ) {
							return response.data;
						} );
					}
					return conferences[id];
				}
			}
		};
		return people;
	}] );