'use strict';

angular.module ( 'Conferences', [ 'REST' ] )
	.controller ( 'ConferenceCtrl', [ '$scope', '$routeParams', 'ConferenceResource', function ( $scope, $routeParams, ConferenceResource ) {

		$scope.data = {};
		$scope.confId = $routeParams.conferenceId;

		ConferenceResource.getById ( $scope.confId ).then ( function ( data ) {
			$scope.data = data;
		} );

		ConferenceResource.getCfps ( $scope.confId ).then ( function ( data ) {
			$scope.data.cfp = data;
		} );

		ConferenceResource.getVenues ( $scope.confId ).then ( function ( data ) {
			$scope.data.venues = data;
		} );

		ConferenceResource.getPublications ( $scope.confId ).then ( function ( data ) {
			$scope.data.publications = data;
		} );


	}] )
	.controller ( 'ConferencesCtrl', [ '$scope', 'ConferenceResource', '$location', function ( $scope, ConferenceResource, $location ) {

		$scope.data = [];
		$scope.page = 0;
		$scope.search = $location.search ();

		$scope.changePage = function ( page ) {
			page = parseInt(page);
			ConferenceResource.getAll ( page ).then ( function ( data ) {
				$scope.data = data;
				$scope.page = page;
				$location.path ( $location.path () ).search ( {page: page} );
			} );
		}

		if ( !$scope.search.page ) {
			$scope.changePage ( $scope.page );
		} else {
			$scope.changePage ( $scope.search.page );
		}

	}] )
	.factory ( 'ConferenceResource', [ '$http', 'resources', function ( $http, resources ) {
		var all = {};
		var ids = {};
		var cfps = {};
		var venues = {};
		var publications = {};
		var conference = {
			getAll         : function ( page ) {
				if ( !page ) {
					page = 0;
				}
				if ( !all[page] ) {
					all[page] = $http.get ( resources.conferences + "?page=" + page ).then ( function ( response ) {
						return response.data;
					} );
				}
				return all[page];
			},
			getById        : function ( id ) {
				if ( id ) {
					if ( !ids[id] ) {
						ids[id] = $http.get ( resources.conferences + "/" + id ).then ( function ( response ) {
							return response.data;
						} );
					}
					return ids[id];
				}
			},
			getCfps        : function ( id ) {
				if ( id ) {
					if ( !cfps[id] ) {
						cfps[id] = $http.get ( resources.conferences + "/" + id + "/calls" ).then ( function ( response ) {
							return response.data;
						} );
					}
					return cfps[id];
				}
			},
			getVenues      : function ( id ) {
				if ( id ) {
					if ( !venues[id] ) {
						venues[id] = $http.get ( resources.conferences + "/" + id + "/venues" ).then ( function ( response ) {
							return response.data;
						} );
					}
					return venues[id];
				}
			},
			getPublications: function ( id ) {
				if ( id ) {
					if ( !publications[id] ) {
						publications[id] = $http.get ( resources.conferences + "/" + id + "/publications" ).then ( function ( response ) {
							return response.data;
						} );
					}
					return publications[id];
				}
			}
		};
		return conference;
	}] );