'use strict';

angular.module ( 'REST', [ ] )
	.factory ( 'resources', [ function () {
		var root = 'http://localhost:9090/core-rest/api/';
		return {
			'cfp'            : root + 'calls',
			'conferences'    : root + 'conferences',
			'publications'   : root + 'publications',
			'plugins'        : root + 'plugins',
			'people'         : root + 'people',
			'venues'         : root + 'venues',
			'recommendations': root + 'recommendations'
		}
	}] );